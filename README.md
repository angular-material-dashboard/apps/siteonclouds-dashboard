# Site on Cloud Dashboard

[![pipeline status](https://gitlab.com/angular-material-dashboard/apps/siteonclouds-dashboard/badges/master/pipeline.svg)](https://gitlab.com/angular-material-dashboard/apps/siteonclouds-dashboard/commits/master)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/5c657c1171784229b0e756ba36401cca)](https://www.codacy.com/app/amd-apps/siteonclouds-dashboard?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=angular-material-dashboard/apps/siteonclouds-dashboard&amp;utm_campaign=Badge_Grade)

Main dashboard of sites on SiteOnClouds.