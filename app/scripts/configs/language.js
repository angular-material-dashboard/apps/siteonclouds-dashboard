/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('siteoncloudsDashboardApp')
//

.config(function($translateProvider) {
    $translateProvider
    //
    .translations('fa', {
	'app.update.message': 'نسخه جدید پنل در دسترس است. پنل به‌روزرسانی خواهد شد.',
	'Search': 'جستجو',
	'Log out': 'خروج',
	
	'Dashboard': 'داشبور',
	'Account': 'حساب کاربری',
	'Profile': 'پروفایل‌ها',
	
	'User management' : 'مدیریت کاربران',
	'Users': 'کاربران',
	'Groups': 'گروه‌ها',
	'Roles': 'نقش‌ها',
	'New user': 'ایجاد کاربر جدید',
	'New group': 'ایجاد گروه جدید',
	'New role': 'ایجاد نقش جدید',
	
	'ID': 'شناسه',
	'Description': 'توضیحات',
	'Name': 'نام',
	'Login': 'لاگین',
	'First name': 'نام',
	'Last name': 'نام خانوادگی',
	'Joind': 'عضویت',
	'Last login': 'آخرین ورود',
	'Last Login': 'آخرین ورود',
	'Joined date': 'زمان ثبت',
	'Joined Date': 'زمان ثبت',
	'Active': 'فعال',
	'EMail': 'پست الکترونیکی',
	'Edit': 'ویرایش',
	'Save': 'ذخیره',
	'Delete': 'حذف',
	'Cancel': 'انصراف',
	'Restore': 'بازیابی',
	'Password': 'گذرواژه',
	'Confirm': 'تایید',

	'Summary': 'خلاصه',
	'Phone': 'شماره تماس',
	'Mobile': 'شماره همراه',
	'LinkedId': 'لینکدین',
	'Telegram': 'تلگرام',
	'Whatsapp': 'واتساپ',
	'Contacts': 'تماس‌ها',
	'User avatar': 'اواتار کاربری',
	'Socials': 'شبکه‌های اجتمائی',
	
	'Content management': 'مدیریت محتوا',
	'Contents': 'محتوا',
	'New Content': 'ایجاد محتوای جدید',
	
	'Spa management': 'مدیریت اپلیکیشن‌ها',
	'Applications': 'اپلیکیشن‌ها',
	'spas': 'اپلیکیشن‌ها',
	'Upload spa': 'بارگزاری اپلیکیشن جدید',
	'Repository': 'مخزن اپلیکیشن‌ها',
	
	'Bank management': 'مدیریت درگاه‌های بانکی',
	'Bank gates': 'درگاه‌های بانکی',
	
	'Discount management': 'مدیریت تخفیف‌ها',
	'Discounts': 'تخفیف‌ها',
	'New Discount': 'ایجاد تخفیف جدید',
	'New discount': 'ایجاد تخفیف جدید',
	
	'Tenant': 'تارنما',
	'Tenant Info': 'اطلاعات تارنما',
	'Tickets': 'درخواست‌ها',
	'Invoices': 'صورتحساب‌ها',
	'Tenant Settings': 'تنظیمات تارنما',
	'Captcha': 'کپچا',
	
	'SEO': 'سئو',
	'Prerender backends': 'موتورهای prerender',
	'New prerender backend': 'موتور prerender جدید',
	'Sitemap links': 'لینک‌های sitemap',
	'New sitemap link': 'اضافه کردن لینک جدید به sitemap',
	
	'Asset management': 'مدیریت محصولات',
	'Assets': 'محصولات',
	'New asset': 'ایجاد محصول جدید',
	'Category': 'دسته',
	'New category': 'ایجاد دسته جدید',
	'Categories': 'دسته‌ها',
	'Subcategories': 'زیر دسته‌ها',
	'Tag': 'برچسب',
	'New tag': 'ایجاد برچسب جدید',
	'Tags': 'برچسب‌ها',
	'Relateds': 'مرتبط',
	
	'Actions': 'اکشن‌ها',
	'Item not found': 'موردی برای نمایش وجود ندارد',
	'Nothing found.': 'موردی یافت نشد.',

	'Events': 'رویدادها',
	
	'change file': 'تغییر فایل',
    });
    $translateProvider.preferredLanguage('fa');
});
